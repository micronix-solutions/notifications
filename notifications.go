/*
Copyright 2020 Micronix Solutions, LLC (https://micronix.solutions/).
Released under the terms of the Modified BSD License
https://opensource.org/licenses/BSD-3-Clause
*/

package notifications

type Notifier interface {
	Notify(message string)
}

func SendNotification(n Notifier, message string) {
	if n != nil {
		go n.Notify(message)
	}
}
